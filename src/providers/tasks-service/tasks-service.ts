import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { SQLiteObject } from '@ionic-native/sqlite';
/*
  Generated class for the TasksServiceProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class TasksServiceProvider {
  db: SQLiteObject = null;
  static lengthRows:number;
  constructor(public http: HttpClient) {
    console.log('Hello TasksServiceProvider Provider');
  }
  setDatabase(db: SQLiteObject){
    if(this.db === null){
      this.db = db;
    }
  }

  create(task: string,task1:string){
    let sql = 'INSERT INTO  tasks(usuario, email) VALUES(?,?)';
    return this.db.executeSql(sql, [task, task1]);
  }

  createTable(){
    let sql = 'CREATE TABLE IF NOT EXISTS tasks(id INTEGER PRIMARY KEY AUTOINCREMENT, usuario TEXT, email TEXT)';
    return this.db.executeSql(sql, []);
  }

  delete(){
    let sql = 'DELETE FROM tasks ';
    return this.db.executeSql(sql, []);
  }

  getAll(){
    let sql = 'SELECT * FROM tasks';
    return this.db.executeSql(sql, [])
    .then(response => {
      let tasks = [];
      for (let index = 0; index < response.rows.length; index++) {
        tasks.push( response.rows.item(index) );
      }
      TasksServiceProvider.lengthRows= response.rows.item(0).size;
      return Promise.resolve( tasks );
      
    })
    .catch(error => Promise.reject(error));
  }
 
 

  update(task: any){
    let sql = 'UPDATE tasks SET title=?, completed=? WHERE id=?';
    return this.db.executeSql(sql, [task.title, task.completed, task.id]);
  }
}
