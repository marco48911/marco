import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { DateTime } from 'ionic-angular';

/*
  Generated class for the UserServiceProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
 export   class UserServiceProvider 
{
  static nombre="unregister";
  url='http://eventoscochabamba.000webhostapp.com/index.php/welcome/registrar';
  urlevento='http://eventoscochabamba.000webhostapp.com/index.php/welcome/registrarevento';
  urlvalidar='http://eventoscochabamba.000webhostapp.com/index.php/welcome/';
  
  constructor(public http: HttpClient ) {
  
  }

   getUsers(nombre:string,password:string ) {
    try 
    { 
      return this.http.get('http://eventoscochabamba.000webhostapp.com/index.php/welcome/get_usuario/'+nombre+'/'+password);
      
    } catch (error) 
    {
      
    }
    //return this.http.get(this.url);//http://localhost:8080/Codeigniter/');
      
  }
  setUser(nombre:string,password:string,correo:string){
    let datos;
    try {
      
      datos=JSON.stringify({
        'nombre' :nombre,
        'password':password,
        'email':correo
       });
    } catch (error) {
      
    }
    this.http.post(this.url,datos)
    .subscribe(data => {
      console.log(data);
     }, error => {
      console.log(error);
    });;
     
   
  }
  setEvento(lat:number,long:number,nombreevento:string,fechainicio:DateTime,fechafinal:DateTime,descripcion:string){
    let datos;
    try {
      
      datos=JSON.stringify({
        'lat' :lat,
        'long':long,
        'nombreevento':nombreevento,
        'fechainicio':fechainicio,
        'fechafinal':fechafinal,
        'descripcion':descripcion
       });
    } catch (error) {
      
    }
    this.http.post(this.urlevento,datos)
    .subscribe(data => {
      console.log(data);
     }, error => {
      console.log(error);
    });;
     
   
  }
  validarRegistro(nombre:string,email:string){
    let datos=JSON.stringify({
      'nombre' :nombre,
      'email':email
     });
    return this.http.post(this.urlvalidar+"existente/",datos);
    
    
    
  }
  verUser(nombre:string)
  {
    try 
    { 
      return this.http.get('http://eventoscochabamba.000webhostapp.com/index.php/welcome/ver/'+nombre);
      
    } catch (error) 
    {
      
    }
    //return this.http.get(this.url);//http://localhost:8080/Codeigniter/');
  }

}
