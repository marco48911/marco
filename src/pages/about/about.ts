import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { UserServiceProvider } from './../../providers/user-service/user-service';
import { ToastController } from 'ionic-angular';

/**
 * Generated class for the AboutPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-about',
  templateUrl: 'about.html',
})
export class AboutPage {
  myParam: string;

  constructor(public navCtrl: NavController, public navParams: NavParams,public userService: UserServiceProvider,private toastCtrl: ToastController) 
  {
    
    
  }

  ionViewDidLoad()
  {
    if(this.navParams.get('myParam')=='unregister')
    {
     
        this.showToastWithCloseButton('Inicia Sesion para poder acceder a todas las caracteristicas');
    }
    else
    {
     
   
    
   }
  }
 

  showToastWithCloseButton(mensaje:string) {
    const toast = this.toastCtrl.create({
      message: mensaje,
      showCloseButton: true,
      closeButtonText: 'Ok',
      duration:3000
    
    });
    toast.onDidDismiss(this.dismissHandler);
    toast.present();
}
private dismissHandler() {
  console.info('Toast onDidDismiss()');
}


}
