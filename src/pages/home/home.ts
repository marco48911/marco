import { Component } from '@angular/core';
import { NavController} from 'ionic-angular';

import { LoadingController } from 'ionic-angular';
import { ToastController } from 'ionic-angular';
import { UserServiceProvider } from './../../providers/user-service/user-service';
import {RegisterPage} from '../register/register';
import {AgendaPage} from '../agenda/agenda';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})

export class HomePage {
  users: any[] = [];
  inputValue: string = "";
  inputValuePassword:string ="";

  constructor(public navCtrl: NavController, public loadingCtrl: LoadingController,private toastCtrl: ToastController,public userService: UserServiceProvider)
  {

  }
  Load()
  {
    try {
      this.userService.getUsers(this.inputValue,this.inputValuePassword).subscribe((data) => 
      { 
        this.users = data['results'];
      
        switch(data['results'])
        {
          case true:
           UserServiceProvider.nombre=this.inputValue;
           this.goToPage2();
          break;

          case false:
          this.showToastWithCloseButton("Nombre de usuario o contraseña incorrectos");
          break;

          default:
          this.showToastWithCloseButton("Porfavor Verifica tu Correo Electronico");
         
        }
      
       
      },
      (error) =>{
       this.showToastWithCloseButton("Error al conectar con el servidor, verifique su conexion a internet");
      }
    )
    
    } 
    catch (error) 
    {
      
    }
    
  }
  goToPage2() {
   
   
      let loading = this.loadingCtrl.create({
        spinner: 'bubbles',
        content: 'Iniciando Sesion...'
      });
  
      loading.present();
  
      setTimeout(() => {
        //this.navCtrl.push(TabsPage,{'myParam':this.inputValue});
        this.navCtrl.setRoot(AgendaPage);
       
        this.inputValue="";
        this.inputValuePassword="";
      }, 3000);
  
      setTimeout(() => {
        loading.dismiss();
      }, 3000);
    
  
     
       
    
   
  }

  showToastWithCloseButton(mensaje:string) {
    const toast = this.toastCtrl.create({
      message: mensaje,
      showCloseButton: true,
      closeButtonText: 'Ok',
      duration:3000
    
    });
    toast.onDidDismiss(this.dismissHandler);
    toast.present();
}
private dismissHandler() {
  console.info('Toast onDidDismiss()');
}


goToRegister()
{
  this.inputValue="";
  this.inputValuePassword="";
  this.navCtrl.push(RegisterPage);
}
 
}



  

