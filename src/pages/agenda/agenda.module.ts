import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AgendaPage } from './agenda';

@NgModule({
  declarations: [
   
  ],
  imports: [
    IonicPageModule.forChild(AgendaPage),
  ],
})
export class AgendaPageModule {}
