import { Component} from '@angular/core';
import { IonicPage, NavController, NavParams,AlertController} from 'ionic-angular';
import { UserServiceProvider } from './../../providers/user-service/user-service';
import { ToastController } from 'ionic-angular';
import { HomePage } from '../home/home';
import { TasksServiceProvider } from '../../providers/tasks-service/tasks-service';
import {RegistereventosPage  } from "../registereventos/registereventos";
/**
 * Generated class for the AgendaPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-agenda',
  templateUrl: 'agenda.html',
})
export class AgendaPage {
  tasks: any[] = [];
  inciado:boolean=false;
  creado:boolean=false;
  myParam: string;
  msg:string="";
users: any[] = [];
text:string='Iniciando Sesion...';
post:any = {color: 'primary'};
data1 = { usuario:"", email:""};
nombre_1:string="";
email_1:string="";
items : any;
letg:number= 0;
enable:boolean=false;
  constructor(public navCtrl: NavController, public navParams: NavParams,public userService: UserServiceProvider,
    private toastCtrl: ToastController,public tasksService:TasksServiceProvider,
    public alertCtrl: AlertController) 
  {
   
    this.getAllTasks();
   
  }
  
  ionViewDidLoad()
  {   
     
      
       if(this.letg>0)
       {
        
        this.msg="Bienvenido a Eventos Cochabamba";
        this.text="Cerrar Sesion";
        this.post.color='danger';
        this.inciado=true
        this.enable=false;
       }
       else
       {
         if (UserServiceProvider.nombre=='unregister')
         { 
       
             this.msg="Inicia Sesion para poder acceder a todas las caracteristicas";
             this.text="Iniciar Sesion";
             this.post.color='primary';
             this.enable=false;
           
         }
         
         else
         {          
             this.userService.verUser(UserServiceProvider.nombre).subscribe(row =>
               { 
                  this.items=JSON.parse(JSON.stringify(row['results']));
                 this.nombre_1= this.items[0].nombre_usuario;
                 this.email_1= this.items[0].email_usuario;
                 this.tasksService.create(UserServiceProvider.nombre,this.email_1);
                 
                 this.getAllTasks();
          
                 this.msg="Bienvenido a Eventos Cochabamba";
                 this.text="Cerrar Sesion";
                 this.post.color='danger';
               });
          
            this.enable=true;
         }
        }
       
  }
 

  showToastWithCloseButton(mensaje:string) {
    const toast = this.toastCtrl.create({
      message: mensaje,
      showCloseButton: true,
      closeButtonText: 'Ok',
      duration:3000
    
    });
    toast.onDidDismiss(this.dismissHandler);
    toast.present();
}
private dismissHandler() {
  console.info('Toast onDidDismiss()');
}
Load()
{
  if(!this.inciado)
  {
    this.navCtrl.push(HomePage);
  }
  else
  {
    
      let alert = this.alertCtrl.create({
        title: 'Cerrar Sesion',
        message: 'Esta Seguro de Cerrar Sesion?',
      
        buttons: [
          {
            text: 'Cancelar',
            handler: () =>{
              console.log('cancelar');
            }
          },
          {
            text: 'Aceptar',
            handler: ()=>{ 
              this.tasks.pop()
              this.tasksService.delete();
              this.getAllTasks();
              this.msg="Inicia Sesion para poder acceder a todas las caracteristicas";
              this.text="Iniciar Sesion";
              this.post.color='primary';
              this.inciado=false;
              this.enable=false;
            }
          }
        ]
      });
      alert.present();
    
   
    

    
  
   
  }
  
}
getAllTasks(){
 this.tasksService.getAll()
 .then(tasks => {
   this.letg=tasks.length; 
  
   if(this.letg>0)
   {
    this.msg="Bienvenido a Eventos Cochabamba";
    this.text="Cerrar Sesion";
    this.post.color='danger';
    this.inciado=true;
    this.enable=true;
   }
   else
   
   {
    this.getAllTasks();
    this.msg="Inicia Sesion para poder acceder a todas las caracteristicas";
    this.text="Iniciar Sesion";
    this.post.color='primary';
    this.inciado=false;
    this.enable=false;
   }
  
   
   this.tasks = tasks;

  
 })
 .catch( error => {
    console.error( error );
});
}

LoadPageRegister()
{
  this.navCtrl.push(RegistereventosPage);
}


}
