import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, DateTime } from 'ionic-angular';
import { ToastController } from 'ionic-angular';
import {
  GoogleMaps,
  GoogleMap,
  GoogleMapsEvent,
  GoogleMapOptions,
  CameraPosition,
  MarkerOptions,
  LatLng,
  Marker,
  
} from '@ionic-native/google-maps';
import { UserServiceProvider } from './../../providers/user-service/user-service';

/**
 * Generated class for the RegistereventosPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-registereventos',
  templateUrl: 'registereventos.html',
})
export class RegistereventosPage {
  map: GoogleMap;
  myLat:any;
  inputValueNombre: string = "";
  inputValueDescripcion:string ="";
  inicio:DateTime;
  final:DateTime;
  
  constructor(public navCtrl: NavController, public navParams: NavParams, private googleMaps: GoogleMaps ,private toastCtrl: ToastController ,public userService: UserServiceProvider) {
  }

  ionViewDidLoad() {
    this.loadMap();
    
  }
  loadMap(){
    let location: LatLng = new LatLng(-17.387215, -66.158771);
    let mapOptions: GoogleMapOptions = {
     
      camera: {
        target: {
         
          lat:  -17.392639, // default location
          lng: -66.158940 // default location
        },
        zoom: 8,
        tilt: 15
      }
    };

    this.map = this.googleMaps.create('map_canvas', mapOptions);
    
    // Wait the MAP_READY before using any methods.
    this.map.one(GoogleMapsEvent.MAP_READY)
    .then(() => {
      // Now you can use all methods safely.
     
      this.map.addEventListener(GoogleMapsEvent.MAP_CLICK).subscribe( row =>{

             this.map.clear();
            this.myLat={lat:row[0].lat,  lng:row[0].lng};
             this.getPosition();
          
         
           
      }
          
        );

         
    })
    .catch(error =>{
      console.log(error);
    });

  }

  getPosition(): void{
    
    this.map.getMyLocation()
    .then(response => {
      this.map.moveCamera({
        target:this.myLat
      });
      this.map.addMarker({
        title: 'Sitio Seleccionado',
        icon: 'red',
        animation: 'DROP',
        position: this.myLat
      });
    })
    .catch(error =>{
      console.log(error);
    });
  }

  showToastWithCloseButton(mensaje:string) {
    const toast = this.toastCtrl.create({
      message: mensaje,
      showCloseButton: true,
      closeButtonText: 'Ok',
      duration:3000
    
    });
    toast.onDidDismiss(this.dismissHandler);
    toast.present();
}
private dismissHandler() {
  console.info('Toast onDidDismiss()');
}
RegisterEvento()
{
 
}

createMarker(loc :LatLng,title:string)
{
  let markerOptions:MarkerOptions={
   position:loc,
   title:title
  };
  return this.map.addMarker(markerOptions);
}
Move(loc:LatLng)
{  if(this.myLat!=null)
  {

   if(this.inputValueNombre=="" || this.inputValueDescripcion=="" || this.inicio==null || this.final==null)
   {
    this.showToastWithCloseButton('Porfavor complete todos los campos');
   }
   else
   {
    this.userService.setEvento(this.myLat.lat,this.myLat.lng,this.inputValueNombre,this.inicio,this.final ,this.inputValueDescripcion);
    this.showToastWithCloseButton("Su solicitud fue enviada y sera repondida en un plazo de 24hrs");
    this.navCtrl.pop();
     
   }
  
  }
  else
  {
    this.showToastWithCloseButton("Porfavor seleccione un lugar en el mapa");
  }
}
}
