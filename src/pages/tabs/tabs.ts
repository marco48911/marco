import { Component } from '@angular/core';
import { NavParams } from 'ionic-angular';
import { AboutPage } from '../about/about';
import {CardsPage} from '../cards/cards';
import {AgendaPage} from '../agenda/agenda';

@Component({
  templateUrl: 'tabs.html'
})
export class TabsPage {

  myParam:string="";
  tab1Root = AboutPage;
  tab2Root = CardsPage;
  tab3Root = AgendaPage;
  
  

  constructor(public navParams: NavParams) 
  {
    this.navParams = navParams;
    
    this.myParam = this.navParams.data;
    
  }
}
