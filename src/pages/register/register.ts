import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { UserServiceProvider } from './../../providers/user-service/user-service';
import {HomePage} from '../home/home';
import { ToastController } from 'ionic-angular';
import { LoadingController } from 'ionic-angular';
import{FormBuilder, FormGroup, Validators,FormControl} from '@angular/forms';

/**
 * Generated class for the RegisterPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-register',
  templateUrl: 'register.html',
})
export class RegisterPage {
  
  myForm:FormGroup;

  constructor(public navCtrl: NavController, public navParams: NavParams,
    public userService: UserServiceProvider,
    private toastCtrl: ToastController, 
    public loadingCtrl: LoadingController,
   
    public formBuilder:FormBuilder  )
  { 
    this.myForm=this.createMyForm();  
   
  }

  createMyForm()
  {
    return this.formBuilder.group({nombre_usuario:['',Validators.compose([Validators.required,Validators.pattern('^([a-z]+[0-9]{0,2}){4,12}$'),Validators.maxLength(12)])],
     email_usuario:['',Validators.compose([Validators.required,Validators.email,this.isValid])],
     password:['',Validators.compose([Validators.required,Validators.minLength(5)])],
     re_password:['',Validators.compose([Validators.required])]
    });
  }
  
saveData()
{
  if(this.myForm.get("password").value==this.myForm.get("re_password").value)
  {
     this.userService.validarRegistro(this.myForm.get("nombre_usuario").value,this.myForm.get("email_usuario").value).subscribe(
       data=>{console.log(data);
       if(data['results'])
       {
          this.userService.setUser(this.myForm.get("nombre_usuario").value,this.myForm.get("password").value,this.myForm.get("email_usuario").value);
          this.goToPage2();
       }
       else
       {
         this.showToastWithCloseButton("El nombre de usuario o correo electronico ya esta registrados");
       }
      
      
      },
      error=>
      {
        this.showToastWithCloseButton("Error al conectar con el servidor, verifique su conexion a internet");
      }
     );
  } 
  else
  {
    this.showToastWithCloseButton('las contraseñas no coinciden')
  }
}


isValid(control: FormControl){
 
  let re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    let result = re.test(control.value);
    
    if (!result) {
      return {
        'email:validation:fail' : true
      }
    }
    
    return null;
}

  showToastWithCloseButton(mensaje:string) {
    const toast = this.toastCtrl.create({
      message: mensaje,
      showCloseButton: true,
      closeButtonText: 'Ok',
      duration:3000
    
    });
    toast.onDidDismiss(this.dismissHandler);
    toast.present();
}
private dismissHandler() {
  console.info('Toast onDidDismiss()');
}


goToPage2() {
   
   
  let loading = this.loadingCtrl.create({
    spinner: 'bubbles',
    content: 'Creando cuenta...'
  });

  loading.present();

  setTimeout(() => {
    this.showToastWithCloseButton('Cuenta creada con exito');
    this.navCtrl.popTo(HomePage);
  }, 3000);

  setTimeout(() => {
    loading.dismiss();
  }, 3000);


 
   


}
}
