import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';
import {HttpClientModule} from '@angular/common/http';
import { UserServiceProvider } from '../providers/user-service/user-service';

import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { AboutPage } from '../pages/about/about';
import {CardsPage} from '../pages/cards/cards';
import {AgendaPage} from '../pages/agenda/agenda';
import {TabsPage} from '../pages/tabs/tabs';
import {RegisterPage} from '../pages/register/register';
import {RegistereventosPage} from '../pages/registereventos/registereventos';
import { UserProvider } from '../providers/user/user';
import { SQLite } from '@ionic-native/sqlite';
import { TasksServiceProvider } from '../providers/tasks-service/tasks-service';
import {EmailComposer} from '@ionic-native/email-composer';
import { GoogleMaps } from '@ionic-native/google-maps';
@NgModule({
  declarations: [
    MyApp,
    HomePage,
    AboutPage,
    CardsPage,
    AgendaPage,
    TabsPage,
    RegisterPage,
    RegistereventosPage
    
   
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    IonicModule.forRoot(MyApp)
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    AboutPage,
    CardsPage,
    AgendaPage,
    TabsPage,
   RegisterPage,
   RegistereventosPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    UserServiceProvider,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    UserProvider,
    SQLite,
    TasksServiceProvider,
    EmailComposer,
    GoogleMaps
  ]
})
export class AppModule {}
