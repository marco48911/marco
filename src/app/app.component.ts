import { Component } from '@angular/core';
import { Platform } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { timer } from 'rxjs/observable/timer';
import { SQLite } from '@ionic-native/sqlite';

import {TabsPage} from '../pages/tabs/tabs';
import { TasksServiceProvider } from '../providers/tasks-service/tasks-service';
@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  rootPage:any = TabsPage;
  showSplash = true; 
  constructor(
    platform: Platform, statusBar: StatusBar, public splashScreen: SplashScreen,public sqlite:SQLite,public tasksService:TasksServiceProvider) {
    platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      statusBar.styleDefault();
      splashScreen.hide();
      this.createDatabase();
      timer(3000).subscribe(() => this.showSplash = false)
    });
  }


  private createDatabase(){
    this.sqlite.create({
      name: 'data.db',
      location: 'default' // the location field is required
    })
    .then((db) => {
      this.tasksService.setDatabase(db);
      return this.tasksService.createTable();
    })
    .then(() =>{
      this.splashScreen.hide();
      this.rootPage = 'TabsPage';
    })
    .catch(error =>{
      console.error(error);
    });
  }
  
}

